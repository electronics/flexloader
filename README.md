*This project used to be hosted the old way on dedicated a webpage. It is probably not useful anymore, but just in case, this is an important of the tarballs as git tags and the webpage as README.md.*


## What is flexloader?

flexloader is a small utility to configure SRAM based ALTERA devices. It downloads a code to the device SRAM using an ALTERA ByteBlaster or a compatible programmer (see schematic below). It dialogs with it through the parallel interface, using parport.

It should support APEX20K, FLEX10K, FLEX6000 and ACEX1K families, though I only tested it on a FLEX10K10 device.

It currently only supports files in raw binary format (.rbf), but others format such as HEX or TTF may be implemented later.


## Schematic

![Schematic](doc/flexloader.png)

## License

The code, documentation and schematics are copyright (C) 2005 [Aurelien Jarno](mailto:aurelien@aurel32.net), and are distributed under the terms of the [GNU General Public License version 2](COPYING).

## Download flexloader

You can download the tarballs directly from the [cgit web interface](../). The pristine-tar branch can be use to recreate the original tarballs using [pristine-tar](https://github.com/pah/pristine-tar).
