/***************************************************************************
                             main.c - Main program
                             ---------------------
			    
    begin                : Sun Jan 09 2005
    copyright            : (C) 2005 by Aurelien Jarno
    email                : aurelien@aurel32.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

/* Application specific includes */
#include "main.h"
#include "flexloader.h"

/* Standard includes */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/shm.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

/***************************************************************************
 *                                                                         *
 *   Declarations                                                          *
 *                                                                         *
 ***************************************************************************/

/* Variables */
int debug = 0;
int quiet = 0;


/***************************************************************************
 *                                                                         *
 *   Functions                                                             *
 *                                                                         *
 ***************************************************************************/

static void print_usage(void)
{
	printf("Usage: "PACKAGE" parport_device rbf_file\n"
	       "\n"
	       "Valid options are:\n"
	       "  -d,       --debug             Print debug messages.\n"
	       "  -h,       --help              Print this message.\n"
	       "  -q,       --quiet             Don't print any message.\n"
	       "  -v,       --version           Display "PACKAGE" version number.\n"
	       "\n");
}

static void print_banner(void)
{
	if (!quiet)
	{
		printf(PACKAGE" version "VERSION"\n");
		printf("(C) 2005 Aurelien Jarno\n\n");
	}
}

int main(int argc, char* argv[])
{
	int c;
	int lindex = 0;
	
	static struct option long_options[] = 
	{
		{"debug",           no_argument,       NULL, 'd'},
		{"help",            no_argument,       NULL, 'h'},
		{"quiet",           no_argument,       NULL, 'q'},
		{"version",         no_argument,       NULL, 'v'},
		{0, 0, 0, 0}
	};      

	while ((c = getopt_long (argc, argv, "dhqv", long_options, &lindex)) != -1)
	{
		printf("%c\n",c);
		switch (c)
		{
			case 'd':
				debug = 1;
				break;
				
			case 'h':
				print_banner();
				print_usage();
				exit(EXIT_SUCCESS);
				break;
				
			case 'q':
				quiet = 1;
				break;
			
			case 'v':
				print_banner();
				exit(EXIT_SUCCESS);	       
				break;

			default:
				print_banner();
				print_usage();
				exit(EXIT_FAILURE);
				break;
		}
	}    
	
	print_banner();
	if (argc - optind != 2)
	{
		print_usage();
		exit(EXIT_FAILURE);
	}    


	if (flex_download_program(argv[optind], argv[optind + 1]) != 0)
	{
		if (!quiet)
			printf("Code NOT downloaded to the FPGA\n\n");
	}
	else
	{
		if (!quiet)
			printf("Code successfully downloaded to the FPGA\n\n");
	}
	
	return EXIT_SUCCESS;
}



