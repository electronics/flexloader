/***************************************************************************
         byteblaster.c - Low-level functions to access the byteblaster
         -------------------------------------------------------------
			    
    begin                : Sun Jan 09 2005
    copyright            : (C) 2005 by Aurelien Jarno
    email                : aurelien@aurel32.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef BYTEBLASTER_H
#define BYTEBLASTER_H

#ifdef HAVE_CONFIG_H
  #include <config.h>
#endif

/* Subroutines */
int open_byteblaster(char *devicename);
void close_byteblaster(int device);
void enable_byteblaster(int device, int value);

void set_DCLK(int device, int value);
void set_nCONFIG(int device, int value);
void set_DATA0(int device, int value);

int get_nSTATUS(int device);
int get_CONF_DONE(int device);

#endif
