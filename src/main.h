/***************************************************************************
                             main.h - Main program
                             ---------------------
			    
    begin                : Mon Nov 24 2003
    copyright            : (C) 2003 by Aurelien Jarno
    emails               : aurelien@aurel32.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MAIN_H
#define MAIN_H

#ifdef HAVE_CONFIG_H
  #include <config.h>
#endif

/* Variables */
extern int debug;
extern int quiet;

#endif
