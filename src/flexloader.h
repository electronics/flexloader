/***************************************************************************
       flexloader.h - Routines to download code to an Altera FLEX FPGA
       ---------------------------------------------------------------
			    
    begin                : Sun Jan 09 2005
    copyright            : (C) 2005 by Aurelien Jarno
    email                : aurelien@aurel32.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FLEXLOADER_H
#define FLEXLOADER_H

#ifdef HAVE_CONFIG_H
  #include <config.h>
#endif

/* Subroutines */
int flex_download_program(char *device, char *rbf_file);

#endif
